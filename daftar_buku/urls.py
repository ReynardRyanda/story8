from django.urls import path
from .views import *

app_name = "daftar_buku"

urlpatterns = [
    path('',daftar_buku, name="daftar_buku"),
    path('cari_buku/<str:param>/', cari_buku, name="cari_buku"),
    path('detail_buku/<str:id>/',detail_buku, name="detail_buku"),
]