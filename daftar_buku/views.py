from django.shortcuts import render
from django.http import JsonResponse
import requests
import json
from .forms import searchbox

# Create your views here.
def daftar_buku(request):
    context = {
        'form' : searchbox()
    }
    return render(request, 'landingpage.html', context)

def cari_buku(request, param):
    json = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + param).json()
    return JsonResponse(json)

def detail_buku(request, id):
    return render(request,"buku.html", {
        'id' : id
    })
