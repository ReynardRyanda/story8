from django.test import TestCase
from django.urls import resolve
import json
import requests
from django.http import JsonResponse
from .views import daftar_buku, detail_buku

# Create your tests here.
class Daftar_Buku_Unit_Test(TestCase):

    def test_url_daftar_buku_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_daftar_buku_html_template_used(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, "landingpage.html")

    def test_daftar_buku_view_function_used(self):
        response = resolve("/")
        self.assertEqual(daftar_buku, response.func)

    def test_Json_for_daftar_buku_is_parsed(self):
        ## test that gives status code 400
        response = self.client.get('/cari_buku/+/')
        self.assertEqual(response.status_code, 200)
        json = requests.get("https://www.googleapis.com/books/v1/volumes?q=+").json()
        self.assertJSONEqual(str(response.content, encoding="utf8"),json)

    def test_url_detail_buku_exist(self):
        response = self.client.get('/detail_buku/1/')
        self.assertContains(response, "Nothing to see here", status_code=200)

    def test_detail_buku_html_template_used(self):
        response = self.client.get('/detail_buku/1/')
        self.assertTemplateUsed(response, "buku.html")

    def test_detail_buku_view_function_used(self):
        response = resolve("/detail_buku/1/")
        self.assertEqual(detail_buku, response.func)
