from django.apps import AppConfig


class DaftarBukuConfig(AppConfig):
    name = 'daftar_buku'
