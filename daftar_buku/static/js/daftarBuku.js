window.addEventListener("DOMContentLoaded", function (event) {
    if (localStorage.length != 0) {
        let content = [];
        Object.keys(localStorage).forEach(function (key) {
            let item = JSON.parse(localStorage.getItem(key));
            content.push(`<div class="card my-3 mx-auto" style="max-width: 18rem"> 
            ${item.volumeInfo.imageLinks ? `<img src="${item.volumeInfo.imageLinks.thumbnail}" class="card-img-top" style="max-width: 18rem" alt="...">` : `<img src="https://upload.wikimedia.org/wikipedia/commons/b/b9/No_Cover.jpg" class="card-img-top" style="max-width: 18rem" alt="...">`}
                <div class="card-body">
                    <h5 class="card-title">${item.volumeInfo.title}</h5>
                    <p class="card-text"><small class="text-muted">${item.volumeInfo.authors ? item.volumeInfo.authors.join(",") : "No author(s) found"}</small></p>
                    <a class="btn btn-dark" href="detail_buku/${item.id}/">See description</a>
                    </div>
                    </div>`)
        });
        document.querySelector(".body").innerHTML = '<div class="card-columns">' + content.join(" ") + '</div>';
    }
})

document.getElementById("searchbox").addEventListener("submit", function (event) {
    event.preventDefault();
    document.querySelector(".body").innerHTML = '<div class="loader"></div>'
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            try {
                localStorage.clear()
            } catch (err) {
                console.log("local storage error")
            }
            if (JSON.parse(this.response).totalItems == 0) {
                document.querySelector(".body").innerHTML = "None";
            } else {
                document.querySelector(".body").innerHTML = '<div class="card-columns">' + JSON.parse(this.response).items.map((item, index, itemsArray) => {
                    try {
                        localStorage.setItem(item.id, JSON.stringify(item));
                    } catch (err) {
                        console.log("local storage error")
                    }
                    return `<div class="card my-3 mx-auto" style="max-width: 18rem"> 
                    ${item.volumeInfo.imageLinks ? `<img src="${item.volumeInfo.imageLinks.thumbnail}" class="card-img-top" style="max-width: 18rem" alt="...">` : `<img src="https://upload.wikimedia.org/wikipedia/commons/b/b9/No_Cover.jpg" class="card-img-top" style="max-width: 18rem" alt="...">`}
                        <div class="card-body">
                            <h5 class="card-title">${item.volumeInfo.title}</h5>
                            <p class="card-text"><small class="text-muted">${item.volumeInfo.authors ? item.volumeInfo.authors.join(",") : "No author(s) found"}</small></p>
                            <a class="btn btn-dark" href="detail_buku/${item.id}/">See description</a>
                            </div>
                            </div>`;
                }).join(" ") + '</div>';
            }
        } else if (this.readyState == 4 && (this.status == 400 || this.status == 404)) {
            document.querySelector(".body").innerHTML = "Awww... something went wrong :("
        }
    };
    param = document.getElementById("id_search_query").value.trim().split(" ").join("_");
    xhttp.open("GET", `cari_buku/${param}/`, true);
    xhttp.setRequestHeader("X-CSRFToken", document.getElementsByName('csrfmiddlewaretoken')[0].value);
    // console.log(document.getElementsByName('csrfmiddlewaretoken')[0].value)
    xhttp.send();
})

