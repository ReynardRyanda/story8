from django import forms

class searchbox (forms.Form):
    search_query = forms.CharField(min_length=1)